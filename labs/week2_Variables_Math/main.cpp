/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 11:29 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Declare two variables of type integer
    int num1, num2;
    
    //Output junk values
    cout << num1 << " " << num2 << endl;
    
    // Intialization
    //num1 =num2 =0;
    num1 = 0;
    num2 = 0;
    
    //Output variables again
    cout << num1 << " " << num2 << endl;
    
    // Get user input or both values
    cout << "Please enter two integers" << endl;
    
    cin >> num1 >> num2;
    
    //cout << "You entered: " << endl;
    //cout << num1 << " " << num2 << endl;
    cout << "You entered: " << endl
            << num1 << " " << num2 <<endl;
    
    // Calculate average of two numbers
    int total = num1 + num2;
    cout << endl << "Total: " << total << endl;
    
    // Case of Integer division
    double averageIntDivision = total / 2;
    
    double averageStaticCast = static_cast<double>(total) / 2;
    double averageDecimal = total / 2.0;
    
    cout << "Average Int Division: " << averageIntDivision
            << endl; // output average
    
    cout << "Average Static Cast: " << averageStaticCast
            << endl; // output average
    
    cout << "Average Decimal: " << averageDecimal
            << endl; // output average
       
    // Modulus operator
    cout << "7 % 3: " << 7 % 3 << endl;
    return 0;
}

