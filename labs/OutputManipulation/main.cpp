/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 10:52 AM
 */

#include <cstdlib>
#include <iostream> // allows me to input/output
#include <iomanip> // 
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // const variable to store a num
    const double randNum = 12.3259;
    
    // error. Can't reassign constant values
    //randNum = 12.5;
    
    //Change num of nums to 2
    cout << setprecision(2);
    
    cout << randNum << endl;
    
    //Output a 3 digit num
    // Only see2 digits wit scientific notation (1.2e+02)
    cout << 120.529 << endl;
    
    // fixing means change decimal places
    cout << fixed << randNum << endl;
    
    //use setw to create columns
    // setw only manipulates very next output
    //setw automatically right justifies
    cout << setw(10) << randNum << randNum << endl;
    
    //using left justification
    cout << setw(10) << left << randNum << randNum << endl;
    
    // using setfill
    cout << setw(10) 
            << setfill ('=')
            << randNum
            << randNum
            << endl;
    
    
    return 0;
}

