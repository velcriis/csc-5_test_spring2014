/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2014, 11:57 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Question one Slugging percentage
    int slug_percent, singles_1, doubles_1, home_runs, triples_1;
    double  at_bats, total_sp; 
    
    cout << "Please enter the number of singles: " << endl;
    cin >> singles_1;
    
    cout << "Please enter the number of doubles: " << endl;
    cin >> doubles_1;
    
    cout << "Please enter the number of triples: " << endl;
    cin >> triples_1;
    
    cout << "Please enter the number of home runs: " << endl;
    cin >> home_runs;
    
    cout << "Please enter the number of at bats: " << endl;
    cin >> at_bats;
    
    slug_percent = singles_1 + 2 * doubles_1 + 3 * triples_1 + 4 * home_runs;
    total_sp = slug_percent / at_bats;
    
    cout << "Your slugging percentage is: " << total_sp << "%" <<endl;
    
    //Question 2
    int num1;
    int num2; 
    
    cout << "Please enter two integers less then 1000: " << endl;
    cin >> num1;
    cin >> num2;
            
    cout << "X = " << num1 << ",  Y = " << num2 << endl;
    
    cout << "X = " << num2 << ", Y = " << num1 << endl;
            
    //Question 3
    int x = 4;
    int y = 0;

        if (x == 4)
        {
        y = 4;
        }
        else if (x == 9)
        {
        y = 5;
        }
        else
        {
        y = 6; 
        }

    
   
    return 0;
}

