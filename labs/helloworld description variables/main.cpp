/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 10:33 AM
 */

#include <cstdlib> //cstdlib library not needed
#include <iostream> // iostream is needed for output

// Gives the context of where my libraries are coming from
using namespace std;

/*
 * 
 */
//there is always and only one main
//Programs always start at main
//Programs execute from top to bottom, and left to right
int main(int argc, char** argv) {
    
    // cout specifies output
    // endl creates a new line
    // << specifies stream opereator
    // All statements end in a semicolon
    
    // Using a programmer-defined identifier/variable
    //message is a variable
    //string is a data type
    // = is an assignment operator
    // Assign right to left
    //string message = "Hello World";
    string message;
    message = "Hello World"; //Variable initialization
    
    cout << message << endl;
    
    // C++ ignored whitespace
    cout
    << 
            "Cristina Velasco";
    
    
    // if program hits return, it ran successfully 
    return 0;
} // All my code is within curly braces

