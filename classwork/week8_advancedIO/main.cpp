/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 22, 2014, 10:56 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    //Use the get function
    //Can use in two ways
    // c = cin.get();
    // or cin.get(c);
    //Get only gets one char
    //Use the cin.get() function to get a single
    //character
    
    cout << "Enter a character: " << endl;
    
    char c;
    c = cin.get(); // or cin.get(c);
    cout << "You character is: " << c << endl;
    
    //Use getline to get a sentence from a user 
    // getline reads until the newline char
    string sentence;
    cout << "Please enter a sentence" << endl;
    cin >> sentence;
    //Getline needs a buffer, and a string as arguments
    // Getline reads up until  the newline char and it then ignore it
    //Void function 
    //Common error to mix getline and stream insertion >>
    // Error with \n (newline) character
    
    getline (cin, sentence);
    
    cout << "Your sentence is: " << sentence <<endl;
         
    
    //Use the stream insertion operator
    // >> reads until whitespace or newline, and then 
    //ignores the char
    
    string word;
    cout << "Please enter a word" << endl;
    
    cin >> word;
    cout << "Your word is: " << word << endl;
    
    // Break my cin buffer
    while(true)
    {
        int number;
        cout << "Enter a number" << endl;
        cin >> number;
        
        cout << " Your number was: " << number << endl;
        
        // Same exact check to see if buffer failed p
        //Same as FILE I/O to see if file failed opening
        if (cin.fail())// need to check if buffer broke
        {
            //First need to clear buffer
            //Then need to ignore
            
            cin.clear(); //resets buffer
            //ignores (clears) the first (256) char
            //or until a newline is found (\n)
            cin.ignore(256, '\n');    
        }
    }
    
    
    return 0;
}

