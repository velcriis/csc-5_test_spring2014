/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 25, 2014, 11:45 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    char play_again;
    string player_1,player_2;
    string r, s, p;

    cout << "Lets play a game of rock, paper, scissors! " << endl;
    cout << "Rock = 'r', Scissors = 's', and Paper = 'p'." << endl;
    cout << "Two people will play. ";
    cout << "I will let you know who won depending on what you picked." << endl;
    
     
    cout << "Ready?? Player ONE...rock, paper, or scissors?" << endl;
    cout << "Remember: Rock = 'r', Scissors = 's', and Paper = 'p'." << endl;
    cin >> player_1;
    
    cout << "Player TWO...rock, paper, or scissors?" << endl;
    cout << "Remember: Rock = 'r', Scissors = 's', and Paper = 'p'." << endl;
    cin >> player_2;
 
    
    if (player_1 == "r" && player_2 == "r" )
        {
        cout << "Tie! Nobody wins..." <<endl;
    }
    else if (player_1 == "s" && player_2 == "s")
    {
        cout << "Tie! Nobody wins..." <<endl;
    }
    else if (player_1 == "p" && player_2 == "p")
    {
        cout << "Tie! Nobody wins..." <<endl;
    }
    else if (player_1 == "r" && player_2 == "s")
    {
        cout << "Player one wins!!! Congratulations!!!" << endl;
    }
    else if (player_1 == "s" && player_2 == "p")
    {
        cout << "Player one wins!!! Congratulations!!!" << endl;
    }
    else if (player_1 == "p" && player_2 == "r")
    {
        cout << "Player one wins!!! Congratulations!!!" << endl;
    }
    else if (player_2 == "r" && player_1 == "s")
    {
        cout << "Player two wins!!! Congratulations!!!" << endl;
    }
    else if (player_2 == "s" && player_1 == "p")
    {
        cout << "Player two wins!!! Congratulations!!!" << endl;
    }
    else if (player_2 == "p" && player_1 == "r")
    {
        cout << "Player two wins!!! Congratulations!!!" << endl;
    }
     
    
    cout << "Will you like to play again?" << endl;
    cout << "Press 'y' if yes, and 'n' if no." << endl;
    
    cin >> play_again;
    
     if (play_again == 'n')
    {
        cout << "Thank you for playing..." ;
    }
        
    while (play_again == 'y')
        
    {
       cout << "Ready?? Player ONE...rock, paper, or scissors?" << endl;
    cout << "Remember: Rock = 'r', Scissors = 's', and Paper = 'p'." << endl;
    cin >> player_1;
    
    cout << "Player TWO...rock, paper, or scissors?" << endl;
    cout << "Remember: Rock = 'r', Scissors = 's', and Paper = 'p'." << endl;
    cin >> player_2;
 
    
    if (player_1 == "r" && player_2 == "r" )
        {
        cout << "Tie! Nobody wins..." <<endl;
    }
    else if (player_1 == "s" && player_2 == "s")
    {
        cout << "Tie! Nobody wins..." <<endl;
    }
    else if (player_1 == "p" && player_2 == "p")
    {
        cout << "Tie! Nobody wins..." <<endl;
    }
    else if (player_1 == "r" && player_2 == "s")
    {
        cout << "Player one wins!!! Congratulations!!!" << endl;
    }
    else if (player_1 == "s" && player_2 == "p")
    {
        cout << "Player one wins!!! Congratulations!!!" << endl;
    }
    else if (player_1 == "p" && player_2 == "r")
    {
        cout << "Player one wins!!! Congratulations!!!" << endl;
    }
    else if (player_2 == "r" && player_1 == "s")
    {
        cout << "Player two wins!!! Congratulations!!!" << endl;
    }
    else if (player_2 == "s" && player_1 == "p")
    {
        cout << "Player two wins!!! Congratulations!!!" << endl;
    }
    else if (player_2 == "p" && player_1 == "r")
    {
        cout << "Player two wins!!! Congratulations!!!" << endl;
    }
     
    
    cout << "Will you like to play again?" << endl;
    cout << "Press 'y' if yes, and 'n' if no." << endl;
    
    cin >> play_again; 
        
    }
    
    
    return 0;
}

