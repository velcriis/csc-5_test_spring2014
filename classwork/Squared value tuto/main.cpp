/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 27, 2014, 10:49 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int value; 
    cin >> value;
    
    // Invoke the square function
    int squaredValue = square(value);
    //Using the variable
    cout << "The square of " << value
            << " is: " << square(value) << endl;
    // Outputting the function call
    cout << "The square of " << value
            << " is: " << square(value) << endl;
    // Using a integer literal
    cout << "The square of " << value
            << " is: " << square(12) << endl;
    int select;
    do
    {
    // Menu-based operation
    cout << "Enter a number: " << endl;
    cout << "1 for square value" << endl;
    cout << "-1 to end" << endl;
    //int select; this has to be declared outside the curly braces
    cin >> select;
    
    switch(select)
    {
        case 1:
            problem();
            break;
        case -1:
            endProgram();
            break;
    }      
    }
    while (select )
    
    return 0;
}

//Define problem 1 function
// Problem 1 function

// Define end program function
