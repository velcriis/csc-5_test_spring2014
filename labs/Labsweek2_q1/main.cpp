/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 12:20 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Hello, my name is Cristina!" << endl;
    cout << "What is your name?" << endl;
    
    string user_name;
            
    cin >> user_name;
    
    cout << "Hello, "<< user_name << ". I am glad to meet you." << endl;
    
    
  
    return 0;
}

