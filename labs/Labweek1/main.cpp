/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 11:37 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip> library
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    
    cout << "Hello, my name is Cristina!" << endl;
    cout << "What is your name?" << endl;
    
    string user_name;
            
    cin >> user_name;
    
    cout << "Hello, "<< user_name << ". I am glad to meet you." << endl;

    int a = 5;
    int b = 10;
    
    int temp = a;
    
    cout << "a: " << a << " b: " << b << endl;
    
    a = b;
    b = temp;
    
    cout << "a: " << a << " b: " << b << endl;
    
    int entered_meter;
    double total_meters;
    double total_meters2;
    double total_meters3;
    double num_miles = 1609.344;
    double num_feet = 3.281;
    double num_inches = 39.372;
    
    cout << "Please enter a measurement in meters." << endl;
    
    cin >> entered_meter;
    
    total_meters = entered_meter * num_miles;
    
    cout << setprecision(2) << fixed;
    cout << entered_meter << "  meters, in miles is:  " << total_meters << endl;
    
    total_meters2 = entered_meter / num_feet;
    
    cout << entered_meter << "  meters, in feet is:  " << total_meters2 << endl; 
    
    total_meters3 = entered_meter / num_inches;
    
    cout << entered_meter << "  meters, in inches is:  " << total_meters3 << endl;
    
    
    return 0;
}

