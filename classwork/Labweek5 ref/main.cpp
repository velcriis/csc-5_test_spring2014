/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 11:34 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //
    int randNum = rand ();
    
    cout << "Number is: " << randNum << endl;
    
    int randNum2 = rand () % 101;
    
    cout << "Number2 is : " << randNum2 << endl;
    
    int max = 1000;
    int min = 100;
    int randNum3 = rand() % (max - min + 1) + min;
    
    cout << "Number 3 is : " << randNum3 << endl;
    
    //Find out how many iterations it takes to get
    // the number 1000
    int counter = 0;
    //counter helps to count for the while loop
    while(randNum3 != 1000 && counter < 100)
    {
        counter++;
        cout << "Iteration: " << counter << endl;
        //we put this to see the infinite loop
        randNum3 = rand () % (max - min + 1) + min;
        //get new random num to stop infinite loop
    }
    
    
    return 0;
}

