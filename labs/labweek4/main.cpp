/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 11:50 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    char play_again;
    srand (time(0));
    do { //Loop through

        cout << "Lets play a guessing game! ";
    cout << "I pick a number and you try to guess it. " << endl;
    int randNum;
    int number = rand () % 100 +1;
    double counter;
    counter = 0;
    int max = 100;
    int min = 1;
//    int randNum3 = rand() % (max - min + 1) + min;
            

    while (randNum != number && counter < 10)
    {
      cout << "Guess a number from 1 to 100: " << endl;
      cin >> randNum;

        
    if (randNum == number)
    {
        cout << "Correct!! Thank you for playing..." << endl;
        
    }
    else if (randNum > number)
    {
        cout << "Incorrect. Your number is too large. " << endl;
        cout << "Guess again." << endl;
 //       cin >> randNum;
 //       randNum3 = rand() % (max - min + 1) + min;
        counter++;
    }
    else if (randNum < number)
    {
//        counter++;
        cout << "Incorrect. Your number is too small. " << endl;
        cout << "Guess again." << endl;
//        cin >> randNum;
//        randNum3 = rand() % (max - min + 1) + min;
        counter++;
    }
    } //End of inner loop (single game)
    
    if( counter >= 10 ) cout << "Ran out of guesses" << endl;
    
    cout << "Would you like to play again?" << endl;
    cout << "If yes press 'y', if no press 'n'. " << endl;
    cin >> play_again;
    
    counter = 0;
    
    } while(play_again == 'y');  //End of outer loop
    
    return 0;
}

