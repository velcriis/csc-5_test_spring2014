/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 12:00 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
 
    int value_1, value_2, total_per_value;
    
    cout << "Hello there..." << endl;
    cout << "Please enter two numbers." << endl;
    
    cin >> value_1;
    cin >> value_2;
    
    total_per_value = value_1 + value_2;
    
    cout << value_1 << " + " << value_2 << " = " << total_per_value << endl;
    
    total_per_value = value_1 * value_2;
    
    cout << value_1 << " * " << value_2 << " = " << total_per_value << endl;
    
    
    return 0;
}

