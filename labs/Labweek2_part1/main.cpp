/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 12:02 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //integer variable and statement
    int y;
    cout << endl;
    cout << "Please enter a number...." << endl;
    
    cin >>y;
    cout <<y << " + 5 = " << y + 5 << endl;
    
    
    // Practice of sting Concatenation
    string var1 = "1";
    int var2 = 2;
    
    cout << var1 << " + " << var1 << " = " << var1 + var1 << endl;
    cout << var2 << " + " << var2 << " = " << var2 + var2 << endl;
    
    //Program prompt the user to take in 3 test scores and output average
    
    int score1, score2, score3, total_score, av_score;
    
    cout << "Please enter three test scores..." << endl;
    
    cout << "Enter first test score: ";
    cin >> score1;
    cout << "Enter second test score: ";
    cin >> score2;
    cout << "Enter third test score: ";
    cin >> score3;
    
    total_score = score1 + score2 + score3;
    
    av_score = total_score / 3;
    
    cout << "Your average is " << av_score << endl;
            
    
    
    return 0;
}

